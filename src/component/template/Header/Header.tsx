import './Header.css'

export const Header = () => {

  return (
    <header className="page-header" style={{boxSizing: 'border-box'}}>
      <b>Interactive</b>
    </header>
  )
}


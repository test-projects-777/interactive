import {IEntry} from '@do-while-for-each/browser-router';
import {IndexPage, TransformCanvas, TransformDom} from '../page';

// @formatter:off
export const routes: IEntry[] = [
  {segment: '', component: <IndexPage/>},
  {segment: 'dom', component: <TransformDom/>},
  {segment: 'canvas', component: <TransformCanvas/>},
];
// @formatter:on

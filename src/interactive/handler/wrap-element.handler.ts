import {share, Stopper, takeUntil} from '@do-while-for-each/rxjs'
import {IStoppable} from '@do-while-for-each/common'
import {DownEvent, MouseLeave, MouseWheel, MoveEvent, UpEvent} from '../event'
import {RectHandler} from './rect.handler'

export class WrapElementHandler implements IStoppable {

  rectHandler = new RectHandler()
  private stopper = new Stopper()

  constructor(public element: Element) {
    this.rectHandler.init(element)
  }

  get down$() {
    return DownEvent.event$(this.element).pipe(
      takeUntil(this.stopper.ob$),
      share(),
    )
  }

  get up$() {
    return UpEvent.event$(this.element).pipe(
      takeUntil(this.stopper.ob$),
      share(),
    )
  }

  get position$() {
    return MoveEvent.event$(this.element).pipe(
      takeUntil(this.stopper.ob$),
      share(),
    )
  }

  get leave$() {
    return MouseLeave.event$(this.element).pipe(
      takeUntil(this.stopper.ob$),
      share(),
    )
  }

  get wheel$() {
    return MouseWheel.event$(this.element).pipe(
      takeUntil(this.stopper.ob$),
      share(),
    )
  }

  stop(): void {
    this.rectHandler.stop()
    this.stopper.stop()
  }

}

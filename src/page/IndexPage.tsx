import {Link} from '@do-while-for-each/browser-router-react-tools'

export const IndexPage = () => {

  return (
    <div>
      <br/>
      <Link href="/dom">DOM</Link><br/><br/>
      <Link href="/canvas">Canvas</Link><br/><br/>
    </div>
  )
}
